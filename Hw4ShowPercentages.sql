DELIMITER //

DROP PROCEDURE IF EXISTS ShowPercentages //

CREATE PROCEDURE ShowPercentages(IN ssnnum VARCHAR(4))
BEGIN
    IF EXISTS(SELECT * FROM Hw4_rawscores WHERE Hw4_rawscores.SSN = ssnum) THEN

        WITH A AS (
            SELECT
                t0.SSN, t0.LName, t0.FName, t0.Section,
                (t0.Quiz1 / t1.Quiz1) AS q1pct,
                (t0.Quiz2 / t1.Quiz2) AS q2pct,
                (t0.Quiz3 / t1.Quiz3) AS q3pct,
                (t0.Quiz4 / t1.Quiz4) AS q4pct,
                (t0.Test1 / t1.Test1) AS t1pct,
                (t0.Test2 / t1.Test2) AS t2pct,
                (t0.Test3 / t1.Test3) AS t3pct
            FROM    
                Hw4_rawscores t0
            LEFT JOIN
                Hw4_rawscores t1
            ON
                t1.SSN = 'TOTL'
            WHERE
                t0.SSN = ssnum
        ), Info AS (
            SELECT A.SSN, A.LName, A.FName, A.Section, A.q1pct, A.q2pct, A.q3pct, A.q4pct, A.t1pct, A.t2pct, A.t3pct, COALESCE(A.q1pct,0) + COALESCE(A.q2pct,0) + COALESCE(A.q3pct,0) + COALESCE(A.q4pct,0) AS "qTotal", COALESCE(A.t1pct,0) + COALESCE(A.t2pct,0) + COALESCE(A.t3pct,0) AS "tTotal"
            FROM A
        )
        SELECT  Info.SSN, Info.LName, Info.FName, Info.Section, Info.q1pct, Info.q2pct, Info.q3pct, Info.q4pct, Info.t1pct, Info.t2pct, Info.t3pct, (Info.qTotal / 4) AS qavg, (Info.tTotal / 3) As tavg
        FROM Info;
    END IF;
END; //

DELIMITER ;


